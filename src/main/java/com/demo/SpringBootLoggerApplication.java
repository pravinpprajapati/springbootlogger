package com.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SpringBootLoggerApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringBootLoggerApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(SpringBootLoggerApplication.class, args);
		
		LOGGER.error("ERROR level log.");
		LOGGER.warn("WARN level log.");
		LOGGER.info("INFO level log.");
		LOGGER.debug("DEBUG level log.");
	}
	
	@RequestMapping("/")
	public String onLoad() {
		
		String s = "myString";
		if(s.length() > 2) {
			throw new RuntimeException("my custome exception has been oddddccured.1234");
		}
		
		return "Hello. This is logger demo.";
	}

}
